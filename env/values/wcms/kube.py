from .base import *
import datetime

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k^j3ora(dh1ij^_sd)o+el)wqic=(rsk^sg@t#q^be5&y$_-_)'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'wcms_alpha2',
        'USER': 'livspace',
        'PASSWORD': 'livspaceadmin',
        'HOST': 'wcmsdev.ciq7fhve3lvr.ap-southeast-1.rds.amazonaws.com',
        'PORT': '3306',
    }
}


#### CLIENTS #####

try:
    from .local import *
except ImportError:
    pass

ENV = 'Dev'
GRAPPELLI_ADMIN_TITLE = 'Livspace Web Admin (Dev)'

# Max File Upload size
DATA_UPLOAD_MAX_MEMORY_SIZE = 524288000


# Elastic search server configuration

AWS_ES_HOST_REGION = 'ap-southeast-1'
AWS_ES_HOST = 'search-lsweb-nkfgkbcz7bcekepwutkfr2ftga.ap-southeast-1.es.amazonaws.com'
AWS_ELASTIC_ACCESS_KEY = 'AKIARPQNMXYNPB5GOP5R'
AWS_ELASTIC_SECRET_KEY = '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'

EVENT_SYSTEM_ENVIRONMENT = 'stage'

ENV = 'alpha2'

JUMANJI_URL = "https://jumanji.alpha2.livspace.com/"

# Logging Properties

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '{asctime} {levelname} {module} {filename} {lineno} - {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'application': {
            'handlers': ['console'],
            'propagate': True,
        },
        'root': {
            'handlers': ['console'],
            'propagate': True,
        },
    },
}
